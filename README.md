# fetch data from matlab figure files

function defined in file get_fig_data

Usage:

```matlab
% fetch data
[x,y] = get_fig_data("figurename.fig");
% first graph
x(:,1)
y(:,1)
% second graph raw data as matrices
x(:,2)
y(:,2)
```