function [x,y]=get_fig_data(figure_name)
    sleep(0.1);
    fig = open(figure_name);
    h = findobj(gca,'Type','line');
    x = cell2mat(get(h,'XData'));
    y = cell2mat(get(h,'YData'));
    close(fig);
    sleep(0.1);
end
